package com.moneygram.cbutility.model;

import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
public class RippleCBRequest {

    String errorType;

    List<RippleError> rippleErrorList;

}
