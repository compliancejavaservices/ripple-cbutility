package com.moneygram.cbutility.model;

import lombok.Data;
import lombok.NonNull;

@Data
public class RippleError {


    @NonNull
    String mgiInfoKey;

    @NonNull
    String errorCode;

    @NonNull
    String errorDescription;

    @NonNull
    String errorType;

    @NonNull
    String alertType;

}
