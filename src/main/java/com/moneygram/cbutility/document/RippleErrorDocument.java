package com.moneygram.cbutility.document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import lombok.Data;
import org.springframework.data.couchbase.core.mapping.Document;

import java.util.List;
import java.util.Map;

@Document
@Data
public class RippleErrorDocument {

    @Id
    String documentId;

    @Field
    private String documentUpdatedTimestamp;

    @Field
    Map<String,RippleObject> rippleObjectMap;

}
