package com.moneygram.cbutility.document;

import com.couchbase.client.java.repository.annotation.Field;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class RippleObject {


    String mgiInfoKey;

    String errorType;

    String errorDescription;

    String errorCode;

    String documentType;

}
