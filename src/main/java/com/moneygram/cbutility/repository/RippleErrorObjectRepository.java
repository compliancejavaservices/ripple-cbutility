package com.moneygram.cbutility.repository;

import com.moneygram.cbutility.document.RippleErrorDocument;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;



public interface RippleErrorObjectRepository extends CouchbaseRepository<RippleErrorDocument,String> {




}
