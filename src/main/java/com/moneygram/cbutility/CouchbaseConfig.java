package com.moneygram.cbutility;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.cluster.ClusterInfo;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.convert.CustomConversions;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.config.CouchbaseConfigurer;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.data.couchbase.core.convert.MappingCouchbaseConverter;
import org.springframework.data.couchbase.core.convert.translation.TranslationService;
import org.springframework.data.couchbase.core.mapping.CouchbaseMappingContext;
import org.springframework.data.couchbase.core.query.Consistency;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;
import org.springframework.data.couchbase.repository.config.RepositoryOperationsMapping;
import org.springframework.data.couchbase.repository.support.IndexManager;
import org.springframework.data.mapping.model.FieldNamingStrategy;

import java.util.List;
import java.util.Set;

@EnableCouchbaseRepositories(basePackages = {"com.moneygram.cbutility"})
@ConfigurationProperties("cb-configuration")
@Configuration
public class CouchbaseConfig extends AbstractCouchbaseConfiguration {


    private String bucketName;

    private String bucketPassword;

    private List<String> bootstrapHosts;

    private long configPollInterval;

    @Override
    protected List<String> getBootstrapHosts() {
        return bootstrapHosts;
    }

    @Override
    protected String getBucketName() {
        return this.bucketName;
    }

    @Override
    protected String getBucketPassword() {
        return this.bucketPassword;
    }

    @Override
    public MappingCouchbaseConverter mappingCouchbaseConverter() throws Exception {
        MappingCouchbaseConverter mappingCouchbaseConverter = super.mappingCouchbaseConverter();

        return mappingCouchbaseConverter;
    }

    @Override
    protected CouchbaseEnvironment getEnvironment() {
        return DefaultCouchbaseEnvironment.builder().configPollInterval(this.configPollInterval).build();
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public void setBucketPassword(String bucketPassword) {
        this.bucketPassword = bucketPassword;
    }

    public void setBootstrapHosts(List<String> bootstrapHosts) {
        this.bootstrapHosts = bootstrapHosts;
    }

    public long getConfigPollInterval() {
        return configPollInterval;
    }

    public void setConfigPollInterval(long configPollInterval) {
        this.configPollInterval = configPollInterval;
    }


}

