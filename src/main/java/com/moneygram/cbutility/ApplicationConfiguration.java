package com.moneygram.cbutility;

import org.apache.catalina.connector.Connector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfiguration.class);

    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> servletContainer(){

        return server -> {
            Connector ajpConnector = ajpConnector();
            if(ajpConnector.getPort()!= -1) {
              server.addAdditionalTomcatConnectors(ajpConnector());
                LOGGER.info("Loaded AJP Properties Successfully");
            }

        };

    }


    @ConfigurationProperties("ajp-connector")
    @Bean
    public Connector ajpConnector() {
        Connector ajpConnector = new Connector("AJP/1.3");
        ajpConnector.setSecure(false);
        ajpConnector.setAllowTrace(false);
        ajpConnector.setScheme("http");
        return ajpConnector;
    }



}
