package com.moneygram.cbutility;

import com.moneygram.cbutility.service.RippleCBUtilityController;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Autowired(required = false)
    BuildProperties buildProperties;


    public Docket api() {

        Docket docket = new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage(RippleCBUtilityController.class.getPackageName())).build();

        docket.apiInfo(apiInfo());
        docket.useDefaultResponseMessages(false);

        return docket;
    }

    public ApiInfo apiInfo() {

        ApiInfo apiinfo = new ApiInfoBuilder().title(" Ripple CBUtility REST API ").description("Ripple CBUtility API Documentation")
                .version(getVersion()).build();
        return apiinfo;
    }

    protected String getVersion() {
        if (buildProperties != null) {
            return buildProperties.getVersion();
        }
        return null;
    }


}
