package com.moneygram.cbutility.exception;

import com.moneygram.api.mgirest_utils.FieldError;
import lombok.Data;

import java.util.List;

@Data
public class ApplicationErrorResponse {

    private String appUri;

    private String message;

    private String transGUID;

    private Integer code;

    private String timeStamp;

    private List<FieldError> fieldErrors;

}
