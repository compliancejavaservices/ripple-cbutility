package com.moneygram.cbutility.service;


import com.moneygram.cbutility.constants.RippleCBUtilityConstants;
import com.moneygram.cbutility.document.RippleErrorDocument;
import com.moneygram.cbutility.exception.ApplicationErrorResponse;
import com.moneygram.cbutility.model.RippleCBDeleteRequest;
import com.moneygram.cbutility.model.RippleCBRequest;
import com.moneygram.cbutility.model.RippleError;
import com.moneygram.cbutility.repository.RippleErrorObjectRepository;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
public class RippleCBUtilityController {

    @Autowired
    RippleCBService rippleCBService;


    @ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal system error", response = ApplicationErrorResponse.class) })
    @RequestMapping(value = "v1/addRippleDQErrorObject", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_XML_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity addRippleDQErrorObject(@RequestBody RippleCBRequest rippleCBRequest) {

        RippleErrorDocument rippleErrorDocument = null;
        ResponseEntity responseEntity = null;
        if (Objects.nonNull(rippleCBRequest)) {

            Boolean response = rippleCBService.addRippleErrorObjectToCB(rippleCBRequest);

            if (response) {
                responseEntity = new ResponseEntity(response, HttpStatus.OK);
            } else {
                responseEntity = new ResponseEntity(response, HttpStatus.NOT_ACCEPTABLE);
            }
        }

        return responseEntity;

    }

    @ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal system error", response = ApplicationErrorResponse.class) })
    @RequestMapping(value = "v1/addRippleFieldMissingErrorObject", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_XML_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity addRippleFieldMissingErrorObject(@RequestBody RippleCBRequest rippleCBRequest) {

        RippleErrorDocument rippleErrorDocument = null;
        ResponseEntity responseEntity = null;
        if (Objects.nonNull(rippleCBRequest)) {

            Boolean response = rippleCBService.addRippleErrorObjectToCB(rippleCBRequest);

            if (response) {
                responseEntity = new ResponseEntity(response, HttpStatus.OK);
            } else {
                responseEntity = new ResponseEntity(response, HttpStatus.NOT_ACCEPTABLE);
            }
        }

        return responseEntity;

    }


    @ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal system error", response = ApplicationErrorResponse.class) })
    @GetMapping(value = "v1/deleteRippleDQErrorObjectById/{id}")
    public ResponseEntity DeleteRippleDQErrorObject( @ApiParam(value = "Delete Ripple Error Object CB key value ", required = true, allowableValues = "RIPPLE:DQ:ERROR,RIPPLE:DC:ERROR")
            @PathVariable("id") String documentID) {

        RippleErrorDocument rippleErrorDocument = null;
        ResponseEntity responseEntity = null;
        if (Objects.nonNull(documentID)){

            String errorType = RippleCBUtilityConstants.MGI_INFO_DQ_KEY_SUFFIX;

            Boolean response = rippleCBService.deleteRippleErrorObjectById(documentID);

            if (response) {
                responseEntity = new ResponseEntity(response, HttpStatus.OK);
            } else {
                responseEntity = new ResponseEntity(response, HttpStatus.NOT_ACCEPTABLE);
            }
        }

        return responseEntity;

    }


    @ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal system error", response = ApplicationErrorResponse.class) })
    @GetMapping(value = "v1/findAllRippleErrorObjects/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RippleErrorDocument> findAllRippleErrorObjects(@ApiParam(value = "Find Ripple Error Object CB key value ", required = true, allowableValues = "RIPPLE:DQ:ERROR,RIPPLE:DC:ERROR")
                                                                             @PathVariable("id") String documentID) {

        RippleErrorDocument rippleErrorDocument = null;
        ResponseEntity responseEntity = null;

         rippleErrorDocument = rippleCBService.findAllByID(documentID);

        if(Objects.nonNull(rippleErrorDocument)) {

            responseEntity = new ResponseEntity(rippleErrorDocument, HttpStatus.OK);

        }else {

            responseEntity = new ResponseEntity(RippleCBUtilityConstants.RIPPLE_OBJECTS_NOT_FOUND, HttpStatus.NOT_ACCEPTABLE);
        }

        return responseEntity;

    }


}
