package com.moneygram.cbutility.service;

import com.moneygram.cbutility.constants.RippleCBUtilityConstants;
import com.moneygram.cbutility.document.RippleErrorDocument;
import com.moneygram.cbutility.document.RippleObject;
import com.moneygram.cbutility.model.RippleCBRequest;
import com.moneygram.cbutility.model.RippleError;
import com.moneygram.cbutility.repository.RippleErrorObjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RippleCBService {

    Logger logger = LoggerFactory.getLogger(RippleCBService.class);

    @Autowired
    RippleErrorObjectRepository rippleErrorObjectRepository;

    public boolean addRippleErrorObjectToCB(RippleCBRequest rippleCBRequest) {


        if (Objects.nonNull(rippleCBRequest.getRippleErrorList()) && rippleCBRequest.getRippleErrorList().size() > 0) {

            RippleErrorDocument rippleErrorDocument = new RippleErrorDocument();
            if (RippleCBUtilityConstants.DQ_ALERT_TYPE.equalsIgnoreCase(rippleCBRequest.getErrorType())) {

                rippleErrorDocument.setDocumentId(RippleCBUtilityConstants.RIPPLE_DQ_ERROR_DOC_TYPE);
                rippleErrorDocument.setDocumentUpdatedTimestamp(getCurrentTimeStampString());
                Map<String, RippleObject> rippleObjectMap = new HashMap<String, RippleObject>();

                rippleCBRequest.getRippleErrorList().forEach(rippleError -> {


                    RippleObject rippleObject = new RippleObject();
                    rippleObject.setMgiInfoKey(rippleError.getMgiInfoKey());
                    rippleObject.setErrorCode(rippleError.getErrorCode());
                    rippleObject.setErrorDescription(rippleError.getErrorDescription());
                    rippleObject.setErrorType(rippleError.getErrorType());
                    rippleObject.setDocumentType(RippleCBUtilityConstants.RIPPLE_DQ_ERROR_DOC_TYPE);

                    rippleObjectMap.put(rippleError.getMgiInfoKey(), rippleObject);

                });
                rippleErrorDocument.setRippleObjectMap(rippleObjectMap);
            } else if (RippleCBUtilityConstants.FIELD_MISSING_ALERT_TYPE.equalsIgnoreCase(rippleCBRequest.getErrorType())) {

                rippleErrorDocument.setDocumentId(RippleCBUtilityConstants.RIPPLE_DC_ERROR_DOC_TYPE);
                rippleErrorDocument.setDocumentUpdatedTimestamp(getCurrentTimeStampString());
                Map<String, RippleObject> rippleObjectMap = new HashMap<String, RippleObject>();

                rippleCBRequest.getRippleErrorList().forEach(rippleError -> {

                    RippleObject rippleObject = new RippleObject();
                    rippleObject.setMgiInfoKey(rippleError.getMgiInfoKey());
                    rippleObject.setErrorCode(rippleError.getErrorCode());
                    rippleObject.setErrorDescription(rippleError.getErrorDescription());
                    rippleObject.setErrorType(rippleError.getErrorType());
                    rippleObject.setDocumentType(RippleCBUtilityConstants.RIPPLE_DC_ERROR_DOC_TYPE);

                    rippleObjectMap.put(rippleError.getMgiInfoKey(), rippleObject);

                });
                rippleErrorDocument.setRippleObjectMap(rippleObjectMap);
            }

            try {


                rippleErrorObjectRepository.save(rippleErrorDocument);
                logger.info(" The Ripple error objects stored in CB is " + rippleErrorDocument);

                return true;
            } catch (Exception e) {

                logger.info(" Exception occurred while saving the Ripple Error Document ");

                return false;
            }

        } else {
            return false;
        }

    }


    public static String getCurrentTimeStampString() {
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    }


    public Boolean deleteRippleErrorObjectById( String documentID) {


        if (Objects.nonNull(documentID)) {

            try {
                    if (rippleErrorObjectRepository.existsById(documentID)) {

                        logger.info(" The mgiInfoKey Ripple error object List to be deleted " + rippleErrorObjectRepository.findById(documentID));

                        rippleErrorObjectRepository.deleteById(documentID);
                    }

            } catch (Exception e) {

                logger.info(" Exception occurred while deleting the ripple error objects from CB ");
            }

        }

        return true;
    }

    public RippleErrorDocument findAllByID(String documentID) {

        Map<String,RippleError> rippleErrorList = new HashMap<String,RippleError>();

        try {

            Optional<RippleErrorDocument> rippleErrorDocumentOptl = rippleErrorObjectRepository.findById(documentID);
            RippleErrorDocument rippleErrorDocument = null;

            if (rippleErrorDocumentOptl.isPresent()) {
                rippleErrorDocument = rippleErrorDocumentOptl.get();
                return rippleErrorDocument;
            }

            logger.info(" Ripple error objects retrieved from CB is " + rippleErrorList);

        } catch (Exception e) {
            logger.info(" Exception occurred while fetching the ripple error objects ");
        }

        return null;
    }
}
