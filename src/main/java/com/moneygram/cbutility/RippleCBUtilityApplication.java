package com.moneygram.cbutility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableConfigurationProperties
public class RippleCBUtilityApplication implements WebMvcConfigurer {

    public static void main(String args[]) {

        SpringApplication application = new SpringApplication(RippleCBUtilityApplication.class);

        application.run(args);

    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        registry.addViewController("/").setViewName("redirect:/swagger-ui.html");

    }

}
