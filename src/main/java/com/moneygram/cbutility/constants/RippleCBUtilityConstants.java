package com.moneygram.cbutility.constants;

public interface RippleCBUtilityConstants {

    public static final String MGI_INFO_DQ_KEY_SUFFIX = "_DQ_KEY";
    public static final String MGI_INFO_DC_KEY_SUFFIX = "_DC_KEY";
    public static final String RIPPLE_DQ_ERROR_DOC_TYPE = "RIPPLE:DQ:ERROR" ;
    public static final String RIPPLE_DC_ERROR_DOC_TYPE = "RIPPLE:DC:ERROR" ;
    public static final String DQ_ALERT_TYPE = "DQ" ;
    public static final String FIELD_MISSING_ALERT_TYPE = "DC" ;
    public static final String RIPPLE_OBJECTS_NOT_FOUND = "Ripple Error Objects not found in CB";

}

